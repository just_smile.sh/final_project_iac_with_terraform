provider "aws" {
  region = local.region
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}
locals {
  region = "us-east-2"
  name = "ddkompik"

    tags = {
    Environment = "dev"
    Owner = "Komarova"
    Name = "ddkompik"
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
   version = "3.12.0"

  name = local.name
  cidr = "10.0.0.0/16"

  azs             = ["${local.region}a", "${local.region}b", "${local.region}c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = false
  one_nat_gateway_per_az = false
  enable_vpn_gateway = false

  enable_ipv6 = false

  manage_default_route_table = true
  default_route_table_tags   = { Name = "${local.name}-default" }

  manage_default_security_group = true
  default_security_group_tags   = { Name = "${local.name}-default" }

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_dhcp_options              = true
  dhcp_options_domain_name         = "service.${local.name}"
  dhcp_options_domain_name_servers = ["20.0.0.1", "30.0.0.1"]

  vpc_tags = {
      Name = "vpc-${local.name}"
  }
}

resource "aws_eip" "nat" {
  count = 3

  vpc = true
}
